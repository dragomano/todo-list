# TODO List
## Демонстрационный проект на Laravel 6

Страница со списком дел, которые можно добавлять, редактировать, удалять.

### Дополнительные компоненты
* laravel/ui (Авторизация)
* laravelcollective/html (Формы)
* fortawesome (Иконки)