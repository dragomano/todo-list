<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskPostRequest;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $tasks = Task::where('user_id', $user_id)->orderBy('id', 'desc')->paginate();

        return view('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskPostRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(TaskPostRequest $request)
    {
        Task::create([
            'user_id' => auth()->user()->id,
            'content' => $request->get('content')
        ]);

        return redirect('/')->with('message', 'Задача добавлена!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $task = Task::findOrFail($id);

        return view('tasks.edit', compact('task'))->with('message', 'Задача изменена!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskPostRequest $request
     * @param int             $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(TaskPostRequest $request, int $id)
    {
        Task::whereId($id)->update([
            'content' => $request->get('content')
        ]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        Task::destroy($id);

        return redirect('/')->with('message', 'Задача удалена!');
    }
}
