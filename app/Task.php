<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];
    protected $perPage  = 3; // Количество задач на странице

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
