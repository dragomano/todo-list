@if ($errors->any())
    <div class="row justify-content-center">
        <div class="col-md-6 mb-1">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif