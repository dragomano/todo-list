@extends('layouts.app')

@section('content')
    @php
        /** @var \Form */
    @endphp
    <div class="container">
        @include('layouts.errors')
        <div class="row justify-content-center">
            <div class="col-md-8 mb-1">
                {{ Form::open(['route' => 'tasks.store']) }}
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            {{ Form::label('content', __('Текст')) }}
                            {{ Form::textarea('content', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <div class="text-center mt-2">
                    {{ Form::submit(__('Сохранить'), ['class' => 'btn btn-primary']) }}
                    <a class="btn btn-secondary" href="{{ route('tasks.index') }}" role="button">{{ __('Отмена') }}</a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
