@extends('layouts.app')

@section('content')
    @php
        /** @var \Form */
    @endphp
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mb-1">
                {{ Form::model($task, ['route' => ['tasks.update', $task->id]]) }}
                <div class="card">
                    @method('PATCH')
                    <div class="card-body">
                        <div class="form-group">
                        {{ Form::label('content', __('Текст')) }}
                        {{ Form::textarea('content', $task->content, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        {{ __('Создано: ') }}{{ $task->created_at }}
                        @if ($task->updated_at > $task->created_at)
                            <span class="float-right">
                                {{ __('Обновлено: ') }}{{ $task->updated_at }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="text-center mt-2">
                    {{ Form::submit(__('Сохранить'), ['class' => 'btn btn-primary']) }}
                    <a class="btn btn-secondary" href="{{ route('tasks.index') }}" role="button">
                        {{ __('Отмена') }}
                    </a>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
