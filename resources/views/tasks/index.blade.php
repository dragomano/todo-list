@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('message'))
        <div class="row justify-content-center mb-2">
            <div class="alert alert-success" role="alert">
                {{ session('message') }}
            </div>
        </div>
        @endif
        <div class="row justify-content-center mb-2">
            <a class="btn btn-primary" href="{{ route('tasks.create') }}" role="button">{{ __('Добавить задачу') }}</a>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10 mb-1">
                <table class="table table-striped table-hover">
                    <tbody>
                    @foreach ($tasks as $item)
                        <tr>
                            <td>{{ $item->content }}</td>
                            <td class="text-right">
                                {{ Form::model($item, ['route' => ['tasks.update', $item->id]]) }}
                                @method('DELETE')
                                @if ($item->user_id === auth()->user()->id)
                                    <a href="{{ route('tasks.edit', [$item->id]) }}" class="btn btn-link"><i class="far fa-edit"></i></a>
                                    <button type="submit" class="btn btn-link"><i class="far fa-trash-alt"></i></button>
                                @endif
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-center">
            {{ $tasks->links() }}
        </div>
    </div>
@endsection
